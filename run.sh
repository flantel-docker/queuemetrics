#!/bin/bash -le

QM_LICENSE=${QM_LICENSE:-none}
QM_MYSQL_HOST=${QM_MYSQL_HOST:-localhost}
QM_MYSQL_USER=${QM_MYSQL_USER:-queuemetrics}
QM_MYSQL_PASS=${QM_MYSQL_PASS:-javadude}
QM_SMTP_HOST=${QM_SMTP_HOST:-localhost}
QM_SMTP_AUTH=${QM_SMTP_AUTH:-false}

cat /tmp/tpf.properties | \
    sed s/QM_LICENSE/"${QM_LICENSE}"/g | \
    sed s/QM_MYSQL_HOST/"${QM_MYSQL_HOST}"/g | \
    sed s/QM_MYSQL_USER/"${QM_MYSQL_USER}"/g | \
    sed s/QM_MYSQL_PASS/"${QM_MYSQL_PASS}"/g | \
    sed s/QM_SMTP_HOST/"${QM_SMTP_HOST}"/g | \
    sed s/QM_SMTP_AUTH/"${QM_SMTP_AUTH}"/g  \
    > /usr/local/queuemetrics/qm-current/WEB-INF/tpf.properties


source  /etc/sysconfig/qm-tomcat6
/usr/local/queuemetrics/tomcat/bin/catalina.sh run

