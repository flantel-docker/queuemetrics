# Queuemetrics Docker

Runs a standard Queuemetrics in CentOS 6.

You can use the following variables to customize.


| **Variable**  | **Description** | **Default** |
| :-------  | :---------- | :------ |
| QM_LICENSE | QM License |  none   |
| QM_MYSQL_HOST | MySQL Host | localhost |
| QM_MYSQL_USER | MySQL User | queuemetrics |
| QM_MYSQL_PASS | MySQL Password | javadude |
| QM_SMTP_HOST | SMTP Host | localhost |
| QM_SMTP_AUTH | SMTP Auth | false | 
| XMS_MEM | java -Xms setting | 1024M |
| XMX_MEM | java -Xmx setting | 4096M |


Example

```
docker run -p 8080:8080 -e QM_LICENSE=xxxx-xxxx-xxxx-xxxx \
-e XMS_MEM=1024M -e XMX_MEM=4096M \
-v /path/to/my/config/configuration.properties:/usr/local/queuemetrics/qm-current/WEB-INF/configuration.properties \
flantel/queuemetrics
```


