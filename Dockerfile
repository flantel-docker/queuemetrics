FROM centos:centos6
MAINTAINER Barry Flanagan <barry@flanagan.ie>

RUN yum -y install wget && \
    wget -P /etc/yum.repos.d http://yum.loway.ch/loway.repo && \
    yum -y install queuemetrics && \
    yum -y clean all
ADD tpf.properties /tmp/tpf.properties
ADD sm_your_logo.gif /usr/local/queuemetrics/qm-current/img/sm_your_logo.gif
ADD logging.properties /usr/local/queuemetrics/tomcat/conf/logging.properties
ADD qm-tomcat6 /etc/sysconfig/qm-tomcat6
ADD run.sh /run.sh
RUN mv /etc/localtime /etc/localtime.bak && ln -s /usr/share/zoneinfo/Europe/Dublin /etc/localtime
CMD ["/run.sh"]
